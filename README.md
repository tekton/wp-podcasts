Tekton WP-Podcasts
==================

A podcast manager built with [Seriously Simple Podcasting](https://wordpress.org/plugins/seriously-simple-podcasting/) that also integrates speakers.
