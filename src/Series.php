<?php namespace Tekton\Wordpress\Podcasts;

class Series extends \Tekton\Support\SmartObject {

    protected $term;
    protected $aliases = array(
        'url' => array('link'),
    );

    function __construct($id) {
        $this->term = \get_term($id, 'series');
        // parent::construct($this->term->term_id, $get_property, $aliases);
    }

    function get_property($key) {
        switch ($key) {
            case 'name': return $this->term->name;
            case 'slug': return $this->term->slug;
            case 'taxonomy': return $this->term->taxonomy;
            case 'url': return \get_term_link($this->term->term_id, 'series');
        }

        throw new \ErrorException('Undefined property, "'.$key.'", on '.self::class);
    }

    function is_valid() {
        return ! empty($this->term);
    }
}
