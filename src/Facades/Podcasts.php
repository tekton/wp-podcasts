<?php namespace Tekton\Wordpress\Podcasts\Facades;

class Podcasts extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'wp.podcasts'; }
}
