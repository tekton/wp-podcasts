<?php namespace Tekton\Wordpress\Podcasts;

class Podcast extends \Tekton\Wordpress\Post {

    function get_property($key) {
        global $ss_podcasting;

        switch ($key) {
            case 'details': return do_shortcode('[podcast_episode episode="'.$this->id.'" content="details"]');
            case 'player': return do_shortcode('[podcast_episode episode="'.$this->id.'" content="player"]');
            case 'recorded': return make_datetime(get_post_meta($this->id, 'date_recorded', true), DATE_ISO);
            case 'download': return $ss_podcasting->get_episode_download_link($this->id);
            case 'series': return $this->series();
            case 'speaker': return $this->speaker();
            case 'thumb': return $this->thumb();
            case 'default_thumb': return asset_url('images/default/podcast-thumb.jpg');
        }

        return parent::get_property($key);
    }

    protected function thumb() {
        if ($this->has_image()) {
            return $this->image;
        }
        else {
            if ($this->has_speaker() && $this->speaker->has_image()) {
                return $this->speaker->image;
            }
            else {
                return image($this->default_thumb);
            }
        }
    }

    protected function speaker() {
        $terms = wp_get_post_terms($this->id, 'podcast_speaker');

        if ( ! empty($terms)) {
            $term = $terms[0];
            return new \Tekton\Wordpress\Podcasts\Speaker($term->term_id);
        }

        return null;
    }

    protected function series() {
        $terms = wp_get_post_terms($this->id, 'series');

        if ( ! empty($terms)) {
            $term = $terms[0];
            return new \Tekton\Wordpress\Podcasts\Series($term->term_id);
        }

        return null;
    }
}
