<?php namespace Tekton\Wordpress\Podcasts;

use Tekton\Wordpress\Image;

class Speaker extends \Tekton\Support\SmartObject {

    protected $term;

    function __construct($id) {
        $this->term = \get_term($id, 'podcast_speaker');
    }

    function get_property($key) {
        switch ($key) {
            case 'name': return $this->term->name;
            case 'url': return get_term_link($this->term->term_id);
            case 'image': return $this->image();
        }
    }

    protected function image() {
        $image = image(term_meta('podcast', 'speaker_thumb_id', $this->term->term_id));

        if ( ! is_null($image) && $image->is_valid()) {
            return $image;
        }

        return image(asset_url('images/default/avatar-thumb.jpg'));
    }

    function has_image() {
        $image = $this->image;

        if ($image instanceof Image && $image->is_valid()) {
            return true;
        }
        else {
            return false;
        }
    }

    function is_image() {
        return $this->has_image();
    }
}
