<?php namespace Tekton\Wordpress\Podcasts\Providers;

use Tekton\Support\ServiceProvider;
use Tekton\Wordpress\Podcasts\PodcastManager;

class PodcastProvider extends ServiceProvider {

    function register() {
        $this->app->singleton('wp.podcasts', function() {
            return new PodcastManager();
        });
    }

    function boot() {
        $this->setupPostType();
        $this->setupMeta();

        // Return
        add_filter('automatic_post_objects', function($post_hijacks) {
            return array_merge($post_hijacks, [
                'podcast' => \Tekton\Wordpress\Podcasts\Podcast::class,
            ]);;
        });

        app('blade')->directive('podcasts', function ($expression) {
            $output = '<?php $query = Podcasts::query('.$expression.') ?>';
            $output .= '<?php while($query->have_posts()) : $podcast = \\__post($query); ?>';

            return $output;
        });

        app('blade')->directive('endpodcasts', function () {
            return "<?php endwhile; \\wp_reset_postdata(); ?>";
        });

        app('blade')->directive('podcastseries', function ($expression) {
            $output = '<?php $query = Podcasts::series('.$expression.') ?>';
            $output .= '<?php while($query->have_posts()) : $podcast = \\__post($query); ?>';

            return $output;
        });

        app('blade')->directive('endpodcastseries', function () {
            return "<?php endwhile; \\wp_reset_postdata(); ?>";
        });

        app('blade')->directive('podcastspeaker', function ($expression) {
            $output = '<?php $query = Podcasts::speaker('.$expression.') ?>';
            $output .= '<?php while($query->have_posts()) : $podcast = \\__post($query); ?>';

            return $output;
        });

        app('blade')->directive('endpodcastspeaker', function () {
            return "<?php endwhile; \\wp_reset_postdata(); ?>";
        });

        add_filter( 'update_post_metadata', [$this, 'setDateRecordedCorrectly'], 10, 5);
    }

    function setDateRecordedCorrectly($check, $object_id, $meta_key, $meta_value, $prev_value) {
        if ($meta_key == 'date_recorded' && ! validateDate($meta_value, DATE_ISO)) {
            // Avoid infinite recursion:
            remove_filter(current_filter(), [$this, 'setDateRecordedCorrectly']);

            // Modify the meta value;
            $meta_value = format_date(make_datetime($meta_value, 'd-m-Y'), DATE_ISO);

            // Update the modified value.
            update_post_meta( $object_id, $meta_key, $meta_value, $prev_value );

            // Return something else than null
            return true;
        }

        return null;
    }

    function setupPostType() {
        add_action('init', function() {
        	register_taxonomy( 'podcast_speaker', 'podcast', array(
        		'hierarchical' => true,    /* if this is false, it acts like tags */
        		'labels' => array(
        			'name' => __( 'Speakers', 'tekton-wp-podcasts' ), /* name of the custom taxonomy */
        			'singular_name' => __( 'Speaker', 'tekton-wp-podcasts' ), /* single taxonomy name */
        			'search_items' =>  __( 'Search Speakers', 'tekton-wp-podcasts' ), /* search title for taxomony */
        			'all_items' => __( 'All Speakers', 'tekton-wp-podcasts' ), /* all title for taxonomies */
        			// 'parent_item' => __( 'Parent Custom Tag', 'tekton-wp-podcasts' ), /* parent title for taxonomy */
        			// 'parent_item_colon' => __( 'Parent Custom Tag:', 'tekton-wp-podcasts' ), /* parent taxonomy title */
        			'edit_item' => __( 'Edit Speaker', 'tekton-wp-podcasts' ), /* edit custom taxonomy title */
        			'update_item' => __( 'Update Speaker', 'tekton-wp-podcasts' ), /* update title for taxonomy */
        			'add_new_item' => __( 'Add New Speaker', 'tekton-wp-podcasts' ), /* add new title for taxonomy */
        			'new_item_name' => __( 'New Speaker', 'tekton-wp-podcasts' ) /* name title for taxonomy */
        		),
        		'show_admin_column' => true,
        		'show_ui' => true,
        		'query_var' => true,
        		'public' => true,
                'show_in_menu' => 'edit.php?post_type=podcast',
        		'rewrite' => array( 'slug' => __('speakers', 'tekton-wp-podcasts' ), 'with_front' => true, 'hierarchical' => false),
        	));
        });
    }

    function setupMeta() {
        add_action( 'metabox_init', function() {
            $podcast_speaker = create_metabox( array(
                'id'           => 'podcast_speaker',
                'title'        => __( 'Podcast Speaker', 'tekton-wp-podcasts' ),
                'object_types' => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
        		'taxonomies'   => array( 'podcast_speaker' ), // Tells CMB2 which taxonomies should have these fields
                'show_names'   => true, // Show field names on the left
            ));
            $image = $podcast_speaker->add_field( array(
                'name'    => __('Speaker Picture', 'tekton-wp-podcasts'),
                'desc'    => 'Upload an image or enter an URL.',
                'id'      => meta_key('podcast', 'speaker_thumb'),
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text' => array(
                    'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
                ),
            ));
        });
    }
}
