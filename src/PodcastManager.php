<?php namespace Tekton\Wordpress\Podcasts;

use WP_Query;

class PodcastManager {

    function query(array $args = []) {
        $queryArgs = array_merge([
            'post_type' => 'podcast',
            'post_status' => 'publish',
            'posts_per_page' => -1,

            // Default, order by date_recorded
            'order' => 'DESC',
            'orderby' => 'meta_value',
            'meta_key' => 'date_recorded',
            'meta_type' => 'DATE',
        ], $args);

        return new WP_Query($queryArgs);
    }

    function series($term, array $args = []) {
        $queryArgs = array_merge([
            'post_type' => 'podcast',
            'post_status' => 'publish',
            'posts_per_page' => -1,

            // Default, order by date_recorded
            'order' => 'DESC',
            'orderby' => 'meta_value',
            'meta_key' => 'date_recorded',
            'meta_type' => 'DATE',

            'tax_query' => [
        		[
        			'taxonomy' => 'series',
        			'field'    => 'slug',
        			'terms'    => $term,
        		],
            ],
        ], $args);

        return new WP_Query($queryArgs);
    }

    function speaker($term, array $args = []) {
        $queryArgs = array_merge([
            'post_type' => 'podcast',
            'post_status' => 'publish',
            'posts_per_page' => -1,

            // Default, order by date_recorded
            'order' => 'DESC',
            'orderby' => 'meta_value',
            'meta_key' => 'date_recorded',
            'meta_type' => 'DATE',

            'tax_query' => [
        		[
        			'taxonomy' => 'podcast_speaker',
        			'field'    => 'slug',
        			'terms'    => $term,
        		],
            ],
        ], $args);

        return new WP_Query($queryArgs);
    }
}
